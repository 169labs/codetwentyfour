'use strict';

const Alexa = require('ask-sdk');
var request = require('request-promise');


//get data from URL with promise
const getData = async url => {
    // Setting URL and headers for request
    var options = {
        url: url,
        json: true,
        headers: {'User-Agent': 'Alexa Skill Backend'}
    };
    
    //console.log(options);
    
     try {
            //console.log("FETCH ", options);
            const result = await request.get(options);
            return result;
        }
        catch (e) {
            if (e.name == "StatusCodeError") {
                if (e.statusCode == 401) {
                    throw new Error("Invalid or unspecified HTTP auth details.");
                }
            }
            //console.error("Error fetching JSON:", e);
            console.error("Error fetching JSON");
            throw e;
        }
    
    
}


const LaunchRequestHandler = {
    
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    async handle(handlerInput) {
        
        

        
        const speechText = 'Welcome to the codeTwentyFour Hackathon! You can say hello to get the bitcoin!';
    
         
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt()
            .getResponse();
        
        
    },
    
   
};

const HelloWorldHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest' && request.intent.name === 'HelloWorldIntent');
  },
  async handle(handlerInput) {
    
    try {
           var data = await getData('https://api.coinmarketcap.com/v1/ticker/');
        } catch (err) {

            console.log(err);
            
            return handlerInput.responseBuilder
            .speak('Ein Fehler ist aufgetreten.')
            .getResponse();
            
        }
        
        console.log(data[0].id);
       
        return handlerInput.responseBuilder
            .speak(data[0].id + ' is currently at ' + data[0].price_usd + ' US Dollar')
            .getResponse();
  },
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log(`Error handled: ${error.message}`);

    return handlerInput.responseBuilder
      .speak('Sorry, I can\'t understand the command. Please say again.')
      .reprompt('Sorry, I can\'t understand the command. Please say again.')
      .getResponse();
  },
};

const skillBuilder = Alexa.SkillBuilders.custom();

exports.main = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    HelloWorldHandler
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();
